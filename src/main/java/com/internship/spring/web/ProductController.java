package com.internship.spring.web;

import com.internship.spring.domain.product.models.ProductCreateDto;
import com.internship.spring.domain.product.models.ProductGetDto;
import com.internship.spring.domain.product.service.ProductService;
import javax.validation.Valid;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/products")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ResponseEntity<ProductGetDto> create(@Valid @RequestBody ProductCreateDto productCreateDto) {
        ProductGetDto productGetDto = productService.create(productCreateDto);
        return new ResponseEntity<>(productGetDto, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Page<ProductGetDto>> get(Pageable pageable) {
        Page<ProductGetDto> allProducts = productService.get(pageable);
        return new ResponseEntity<>(allProducts, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductGetDto> getById(@PathVariable Long id) {
        ProductGetDto productGetDto = productService.getById(id);
        return new ResponseEntity<>(productGetDto, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductGetDto> update(@PathVariable Long id,
        @Valid @RequestBody ProductCreateDto productCreateDto) throws Exception{
        ProductGetDto productGetDto = productService.update(productCreateDto, id);
        return new ResponseEntity<>(productGetDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) throws Exception {
        productService.delete(id);
    }
}
