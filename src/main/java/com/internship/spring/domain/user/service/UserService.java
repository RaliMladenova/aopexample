package com.internship.spring.domain.user.service;

import com.internship.spring.domain.user.entity.User;
import com.internship.spring.domain.user.models.UserCreateDTO;
import com.internship.spring.domain.user.models.UserGetDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {

    UserGetDTO create(UserCreateDTO user);

    User getById(Long id);

    Page<User> get(Pageable pageable);

    void delete(Long id);
}
