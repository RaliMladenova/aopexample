package com.internship.spring.domain.user.service;

import com.internship.spring.domain.user.entity.User;
import com.internship.spring.domain.user.models.UserCreateDTO;
import com.internship.spring.domain.user.models.UserGetDTO;
import com.internship.spring.domain.user.repository.UserRepository;
import com.internship.spring.infrastructure.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserGetDTO create(UserCreateDTO userDto) {
        User user = userMapper.userCreateDtoToUser(userDto);

        User createdUser = userRepository.save(user);

        return userMapper.userToUserGetDto(createdUser);
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id).orElseThrow();
    }

    @Override
    public Page<User> get(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public void delete(Long id) {
        if (!userRepository.existsById(id)) {
            throw new IllegalArgumentException();
        }
        userRepository.deleteById(id);
    }
}
