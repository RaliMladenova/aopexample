package com.internship.spring.domain.user.models;

public record UserGetDTO(
        Long id,
        String firstName,
        String lastName,
        String email,
        int age) {
}
