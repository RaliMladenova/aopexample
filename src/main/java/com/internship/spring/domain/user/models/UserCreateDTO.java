package com.internship.spring.domain.user.models;

import javax.validation.constraints.*;

public record UserCreateDTO(
        @NotBlank(message = "First name should not be blank")
        @Size(min= 3, message = "Length of the first name must be at least 3 chars long")
        String firstName,

        @NotBlank
        @Size(min = 3)
        String lastName,

        @Email
        String email,

        int age,

        @NotBlank
        String password) {
}
