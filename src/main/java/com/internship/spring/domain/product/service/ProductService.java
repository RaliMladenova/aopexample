package com.internship.spring.domain.product.service;

import com.internship.spring.domain.product.models.ProductCreateDto;
import com.internship.spring.domain.product.models.ProductGetDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;

public interface ProductService {
    ProductGetDto create(ProductCreateDto productCreateDto);
    Page<ProductGetDto> get(Pageable pageable);
    ProductGetDto getById(Long id);
    ProductGetDto update(ProductCreateDto productCreateDto, Long id) throws Exception;
    void delete(Long id) throws Exception;
}
