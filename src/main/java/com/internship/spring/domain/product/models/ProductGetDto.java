package com.internship.spring.domain.product.models;

public record ProductGetDto(
    Long id,
    String name,
    double price
) {

}
