package com.internship.spring.domain.product.service;

import com.internship.spring.domain.product.entity.Product;
import com.internship.spring.domain.product.models.ProductCreateDto;
import com.internship.spring.domain.product.models.ProductGetDto;
import com.internship.spring.domain.product.repository.ProductRepository;
import com.internship.spring.infrastructure.mappers.ProductMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public ProductGetDto create(ProductCreateDto productCreateDto) {
        Product product = productMapper.productCreateDtoToProduct(productCreateDto);
        return productMapper.productToProductGetDto(productRepository.save(product));
    }

    @Override
    public Page<ProductGetDto> get(Pageable pageable) {
        return productRepository.findAll(pageable)
            .map(productMapper::productToProductGetDto);
    }

    @Override
    public ProductGetDto getById(Long id)  {
        Product product = productRepository.findById(id).orElseThrow();
        return productMapper.productToProductGetDto(product);
    }

    @Override
    public ProductGetDto update(ProductCreateDto productCreateDto, Long id) throws Exception{
        Product product = productRepository.findById(id).orElseThrow(()
            -> new NotFoundException());
        productMapper.updateProductFromDto(productCreateDto, product);
        return productMapper.productToProductGetDto(productRepository.save(product));
    }

    @Override
    public void delete(Long id) throws Exception {
        if(!productRepository.existsById(id))
            throw new NotFoundException();
        productRepository.deleteById(id);
    }
}
