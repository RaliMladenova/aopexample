package com.internship.spring.domain.product.models;

import javax.validation.constraints.NotBlank;

public record ProductCreateDto(
    @NotBlank
    String name,
    double price
) {

}
