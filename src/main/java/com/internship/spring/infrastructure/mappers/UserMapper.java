package com.internship.spring.infrastructure.mappers;

import com.internship.spring.domain.user.entity.User;
import com.internship.spring.domain.user.models.UserCreateDTO;
import com.internship.spring.domain.user.models.UserGetDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    User userCreateDtoToUser(UserCreateDTO userCreateDTO);

    UserGetDTO userToUserGetDto(User user);
}
