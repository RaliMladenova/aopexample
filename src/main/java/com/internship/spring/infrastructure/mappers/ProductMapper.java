package com.internship.spring.infrastructure.mappers;

import com.internship.spring.domain.product.entity.Product;
import com.internship.spring.domain.product.models.ProductCreateDto;
import com.internship.spring.domain.product.models.ProductGetDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    Product productCreateDtoToProduct(ProductCreateDto productCreateDto);
    ProductGetDto productToProductGetDto(Product product);
    void updateProductFromDto(ProductCreateDto productCreateDto, @MappingTarget Product product);
}
