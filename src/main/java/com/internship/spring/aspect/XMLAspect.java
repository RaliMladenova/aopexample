package com.internship.spring.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class XMLAspect {
    public void myBeforeAdvice(JoinPoint jp)
    {
        System.out.println("Before advice");
    }

    public void myAfterAdvice(JoinPoint jp)
    {
        System.out.println("After advice");
    }
    public void myAroundAdvice(JoinPoint jp)
    {
        System.out.println("Around advice");
    }
    private void myAfterThrowAdvice(JoinPoint jp)
    {
        System.out.println("An exception was thrown");
    }
    private void myReturnAdvice(JoinPoint jp)
    {
        Object[] args = jp.getArgs();
        System.out.println("The returned value is with id: " + args[0]);
    }


}
