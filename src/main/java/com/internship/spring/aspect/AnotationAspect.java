package com.internship.spring.aspect;

import com.internship.spring.domain.product.models.ProductCreateDto;
import com.internship.spring.domain.product.models.ProductGetDto;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AnotationAspect {

    @Pointcut("execution(* com.internship.spring.domain.product.service.ProductService.*(..)) ")
    private void anyProductService() {}
    @After("execution(* com.internship.spring.domain.product.service.ProductService.create(..)) ")
    public void afterAdvice(JoinPoint joinPoint)
    {
        Object[] args = joinPoint.getArgs();
        System.out.println(
            "After method:" + joinPoint.getSignature()
            + "\n "
            + "Added " + args[0]);
    }
    @Before("execution(* com.internship.spring.domain.product.service.ProductService.create(..)) ")
    public void beforeAdvice(JoinPoint joinPoint)
    {
        Object[] args = joinPoint.getArgs();
        System.out.println(
            "Before method:" + joinPoint.getSignature()
                + "\n "
                 + args[0] + " will be added");
    }
    @AfterThrowing("anyProductService()")
    public void afterThrowingAdvice(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        System.out.println("Exception thrown in method: " + joinPoint.getSignature());
    }

    @AfterReturning(pointcut="execution(* getById(..)) && args(id)", returning="returnedDto")
    public void afterReturningAdvice(ProductGetDto returnedDto, Long id) {
        System.out.println("afterReturningAdvice executed. Result: "+returnedDto);
    }
    @Around("execution(* com.internship.spring.domain.product.service.ProductService.update(..)) "
        + "&& args(productCreateDto, id)")
    public void aroundAdvice(ProductCreateDto productCreateDto, Long id) {
        System.out.println("aroundAdvice executed. Product with id: " + id);
    }
}
