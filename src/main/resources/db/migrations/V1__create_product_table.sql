CREATE TABLE product(
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(MAX) NOT NULL,
    price decimal NOT NULL
)